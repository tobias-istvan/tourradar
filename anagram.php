<?php

/**
 * Get all the alphanumeric characters (letters) from the given string with the information about the occurence's of the letters
 * @param String $str The given string
 * @return Array An associative array where the keys are the letters and the values are the occurence of the letters
 */
function getLettersStatFromStr(string $str): array
{
	$str = mb_strtolower(preg_replace("/[^A-Za-z0-9]/", '', $str));
	$strArray = str_split($str);
	$letters = [];
	for ($i=0; $i<count($strArray); $i++) {
		if (!isset($letters[$strArray[$i]])) {
			$letters[$strArray[$i]] = 0;
		}
		$letters[$strArray[$i]] += 1;
	}
	ksort($letters);
	return $letters;
}

/**
 * Checks if the given strings are anagrams for eachothers
 * @param string $str1 The first string to check
 * @param string $str2 The second string to check
 * @return bool Return FALSE if they are not and return TRUE if they are anagrams
 */
function isAnagram(string $str1, string $str2): string
{
	$letters1 = getLettersStatFromStr($str1);
	$letters2 = getLettersStatFromStr($str2);
	if (count($letters1) !== count($letters2)) {
		return FALSE;
	}
	foreach ($letters1 as $letter => $count) {
		if (isset($letter) AND (!isset($letters2[$letter]) OR $letters1[$letter] !== $letters2[$letter])) {
			return FALSE;
		}
	}
	return TRUE;
}

/***
	Tests
***/

$tests = [
	["admirer", "married"],
	["AstroNomers", "no more stars"],
	["string1", "string2"]
];

for ($i=0;$i<count($tests);$i++) {
	$words = $tests[$i];
	echo $words[0] . " and " . $words[1] . " is " . (isAnagram($words[0], $words[1]) ? '' : 'not') . " anagram\n";
}
