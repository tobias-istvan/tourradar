<?php

$text = '<?xml version="1.0"?>
<TOURS>
    <TOUR>
        <Title><![CDATA[Anzac &amp; Egypt Combo Tour]]></Title>
        <Code>AE-19</Code>
        <Duration>18</Duration>
        <Start>Istanbul</Start>
        <End>Cairo</End>
        <Inclusions>
            <![CDATA[<div style="margin: 1px 0px; padding: 1px 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; text-align: justify; line-height: 19px; color: rgb(6, 119, 179);">The tour price&nbsp; cover the following services: <b style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;">Accommodation</b>; 5, 4&nbsp;and&nbsp;3 star hotels&nbsp;&nbsp;</div>]]>
        </Inclusions>
        <DEP DepartureCode="AN-17" Starts="04/19/2015" GBP="1458" EUR="1724" USD="2350" DISCOUNT="15%" />
        <DEP DepartureCode="AN-18" Starts="04/22/2015" GBP="1558" EUR="1784" USD="2550" DISCOUNT="20%" />
        <DEP DepartureCode="AN-19" Starts="04/25/2015" GBP="1558" EUR="1784" USD="2550" />
    </TOUR>
    <TOUR>
        <Title><![CDATA[Anzac &amp; Egypt Combo Tour]]></Title>
        <Code>AE-19</Code>
        <Duration>18</Duration>
        <Start>Istanbul</Start>
        <End>Cairo</End>
        <Inclusions>
            <![CDATA[<div style="margin: 1px 0px; padding: 1px 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; text-align: justify; line-height: 19px; color: rgb(6, 119, 179);">The tour price&nbsp; cover the following services: <b style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;">Accommodation</b>; 5, 4&nbsp;and&nbsp;3 star hotels&nbsp;&nbsp;</div>]]>
        </Inclusions>
        <DEP DepartureCode="AN-17" Starts="04/19/2015" GBP="1458" EUR="1724" USD="2350" DISCOUNT="15%" />
        <DEP DepartureCode="AN-18" Starts="04/22/2015" GBP="1558" EUR="1784" USD="2550" DISCOUNT="20%" />
        <DEP DepartureCode="AN-19" Starts="04/25/2015" GBP="1558" EUR="1784" USD="2550" />
    </TOUR>
</TOURS>';

function getHeaders($row)
{
	$headers = [];
	foreach ($row as $title => $val) {
		if ($title !== 'DEP') {
			array_push($headers, $title);
		}
	}
	array_push($headers, 'MinPrice');
	return $headers;
}

function convertHtmlSpecialChars($str)
{
	return str_replace('&nbsp;', ' ', htmlspecialchars_decode($str));
}

function getValues($row, $fields)
{
	$values = [];
	for ($i=0; $i<count($fields); $i++) {
		$field = $fields[$i];
		if (isset($row->$field)) {
			$val = $row->$field;
		} elseif (is_callable('get' . $field)) {
			$val = call_user_func('get' . $field, $row);
		} else {
			throw new Exception("Invalid field: " . $field, 1);
		}
		switch ($field) {
			case 'Duration':
				$val = intval($val);
			break;
			case 'Inclusions':
				$val = convertHtmlSpecialChars(strip_tags($val), ENT_QUOTES);
			break;
			case 'Title':
				$val = convertHtmlSpecialChars($val);
			break;
		}
		array_push($values, $val);
	}
	return $values;	
}

function getMinPrice($row)
{
	$minPrice = null;
	for ($i=0; $i<count($row->DEP); $i++) {
		$price =  number_format(floatval($row->DEP[$i]->attributes()['EUR']), 2);
		if ($minPrice === null OR $price < $minPrice) {
			$minPrice = $price;
		}
	}
	return $minPrice;
}

function xmlToCSV(string $text, string $separator = '|', array $fields = ['Title','Code','Duration','Inclusions', 'MinPrice']): string
{
	$xml = simplexml_load_string($text);
	$csv = [];
	$tours = $xml->TOUR;
	for ($i=0; $i<count($tours); $i++) {
		$tour = $tours[$i];
		if ($i === 0) {
			array_push($csv, implode($separator, $fields));
		}
		array_push($csv, implode($separator, getValues($tour, $fields)));
	}
	return implode("\n", $csv);
}

echo xmlToCSV($text);
